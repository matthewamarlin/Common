﻿using System.Collections.Generic;

namespace SpearOne.Common.Extensions
{
    public static class DictionaryExtensions
    {
        /// <summary>
        ///     Adds a new item or updates it if it exists.
        /// </summary>
        /// <param name="dictionary">The dictionary to add or update</param>
        /// <param name="key">The dictionary key to add or update</param>
        /// <param name="value">The dictionary value to add or update</param>
        public static void AddOrUpdate<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.ContainsKey(key))
                dictionary[key] = value;
            else
                dictionary.Add(key, value);
        }
    }
}
