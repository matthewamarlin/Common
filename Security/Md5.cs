﻿using System.Security.Cryptography;
using System.Text;

namespace SpearOne.Common.Security
{
    public static class Md5
    {
        public static string ComputeHash(string input)
        {
            // step 1, calculate MD5 hash from input
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            var result = new StringBuilder();
            foreach (var @byte in hash)
            {
                result.Append(@byte.ToString("X2"));
            }

            return result.ToString();
        }

    }
}
