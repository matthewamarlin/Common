﻿using System.Security.Cryptography;

namespace SpearOne.Common.Security
{
    /// <summary>
    /// HMAC based OTP(one time password) generator.  
    /// See RFC 4226 and work by the OATH initiative
    /// </summary>
    public static class HmacOtpGenerator
    {

        /// <summary>
        /// Generates a One Time Password based on HMAC - SHA1
        /// </summary>
        /// <param name="secret">shared secret</param>
        /// <param name="movingFactor">a value that changes on per use basis, counter or date time </param>
        /// <param name="codeDigits">number of digits in otp</param>
        /// <param name="addChecksum">flag to indicate if a check sum should be apended to the otp</param>
        /// <param name="truncationOffset">offset into the calculated MAC to begin truncation</param>
        /// <returns></returns>
        public static string GenerateOtp(byte[] secret,
                                         long movingFactor,
                                         int codeDigits,
                                         bool addChecksum,
                                         int truncationOffset)
        {

            var hmac = new HMACSHA1(secret);

            // put movingFactor value into text byte array
            var digits = addChecksum ? (codeDigits + 1) : codeDigits;

            var text = new byte[8];
            for (var i = text.Length - 1; i >= 0; i--)
            {
                text[i] = (byte)(movingFactor & 0xff);
                movingFactor >>= 8;
            }

            // compute hmac hash
            var hash = hmac.ComputeHash(text);

            // put selected bytes into result int
            var offset = hash[hash.Length - 1] & 0xf;

            if ((0 <= truncationOffset) &&
                (truncationOffset < (hash.Length - 4)))
            {
                offset = truncationOffset;
            }

            var binary =
               ((hash[offset] & 0x7f) << 24)
               | ((hash[offset + 1] & 0xff) << 16)
               | ((hash[offset + 2] & 0xff) << 8)
               | (hash[offset + 3] & 0xff);

            var otp = binary % DigitsPower[codeDigits];

            if (addChecksum)
            {
                otp = (otp * 10) + CalcChecksum(otp, codeDigits);
            }

            var result = otp.ToString();

            while (result.Length < digits)
            {
                result = "0" + result;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <param name="digits"></param>
        /// <returns></returns>
        public static int CalcChecksum(long num, int digits)
        {
            var doubleDigit = true;
            var total = 0;
            while (0 < digits--)
            {
                var digit = (int)(num % 10);
                num /= 10;
                if (doubleDigit)
                {
                    digit = DoubleDigits[digit];
                }
                total += digit;
                doubleDigit = !doubleDigit;
            }
            var result = total % 10;
            if (result > 0)
            {
                result = 10 - result;
            }
            return result;
        }

        // These are used to calculate the check-sum digits.
        //  0  1  2  3  4  5  6  7  8  9
        private static readonly int[] DoubleDigits = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };

        //  0  1  2  3  4  5  6  7  8
        private static readonly int[] DigitsPower
                      = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000 };

    }
}
