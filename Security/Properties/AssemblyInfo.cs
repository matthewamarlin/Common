﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Common.Security")]
[assembly: AssemblyDescription("This package contains common code for Security, such as Encryption and OTP Generators.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("76aa3eb8-62f2-4a29-94eb-f2286242304d")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]