﻿using System.Diagnostics;

namespace SpearOne.Common.Windows
{
    public static class WindowsFirewall
    {
        public static void AddException(string path, string programName)
        {
            //add application to the firewall exception list
            var processStartInfo = new ProcessStartInfo
            {
                FileName = "netsh",
                Arguments = "firewall add allowedprogram " + path + " " + programName + " ENABLE",
                UseShellExecute = false,
                CreateNoWindow = true
            };

            Process.Start(processStartInfo);
        }
    }
}
