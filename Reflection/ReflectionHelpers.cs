﻿using System;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;

namespace SpearOne.Common.Reflection
{
    public static class ReflectionHelpers
    {
        /// <summary>
        /// Determines if the given object is a type belongs to the CLR.
        /// </summary>
        /// <param name="object">The object to check</param>
        /// <returns>A bool specifying if the type belongs to the CLR. True if it is a CLR type, False otherwise.</returns>
        public static bool IsSystemType(this object @object)
        {
            return @object.GetType().IsSystemType();
        }

        /// <summary>
        ///  Determines if the given type belongs to the CLR.
        /// </summary>
        /// <param name="type">The type to check</param>
        /// <returns>A bool specifying if the type belongs to the CLR. True if it is a CLR type, False otherwise.</returns>
        public static bool IsSystemType(this Type @type)
        {
            if (type.IsPrimitive || type.Namespace == "System" ||
                type.Module.ScopeName == "CommonLanguageRuntimeLibrary")
            {
                return true;
            }

            return type.Namespace != null && type.Namespace.StartsWith("System");
        }

        /// <summary>
        ///    This function is used to transerve the object graph of the given object and apply an action to them. 
        ///    This is done by recursively inspecting the public properties of the given object.
        /// 
        ///     Conditions:
        ///     1. The function will not step into the properties of any CLR Types.
        ///     2. The function checks if properties refer back to the current objects parent, if so, these properties are ignored.
        ///     3. In the special case of encountering a type of IEnumerable, the function will enumerate over the members of the collection.
        ///     4. In the special case of a Dictionary the function will enumerate the KeyValue pairs and inspect both Key and Value.
        ///     
        ///     Prerequisites:
        ///     1. The Type must be a class.
        /// </summary>
        /// <typeparam name="TObject">The Type of the object to be recursed</typeparam>
        /// <param name="object">The instance of the object ot be recursed</param>
        /// <param name="action">An action to apply to a given Type and the actual instance of that Type.</param>
        /// <param name="actionPredicate">A predicate accepting an instance of the current object which returns a bool signalling to the function that the action should be applied.</param>

        public static void RecursivelyApplyAction<TObject>(this TObject @object, Action<Type, object> action, Expression<Func<object, bool>> actionPredicate)
            where TObject : class
        {
            RecursivelyApplyAction(@object, action, actionPredicate, type => true);
        }

        /// <summary>
        ///  Iterates the Key and Value of a dictionary calling <see cref="RecursivelyApplyAction&lt;TObject&gt;"/> on each member.
        /// </summary>
        /// <param name="dictionary">The dictionary to iterate</param>
        /// <param name="action">The action to apply</param>
        /// <param name="actionPredicate">A predicate accepting an instance of the current object which returns a bool signalling to the function that the action should be applied.</param>
        /// <param name="recursePredicate">A predicated accepting a Type and returning a bool which signal to the function if recursion should continue.</param>
        public static void RecursivelyApplyAction(this IDictionary dictionary, Action<Type, object> action,
                                    Expression<Func<object, bool>> actionPredicate, Expression<Func<Type, bool>> recursePredicate)
        {
            foreach (var keyValuePair in dictionary)
            {
                var key = keyValuePair.GetType().GetProperty("Key").GetValue(keyValuePair, null);
                var value = keyValuePair.GetType().GetProperty("Value").GetValue(keyValuePair, null);

                RecursivelyApplyAction(key, action, actionPredicate, recursePredicate);
                RecursivelyApplyAction(value, action, actionPredicate, recursePredicate);
            }
        }

        /// <summary>
        ///  Iterates the members of a IEnumerable calling <see cref="RecursivelyApplyAction&lt;TObject&gt;"/> on each member.
        /// </summary>
        /// <param name="enumerable">The IEnumerable to iterate</param>
        /// <param name="action">The action to apply</param>
        /// <param name="actionPredicate">A predicate accepting an instance of the current object which returns a bool signalling to the function that the action should be applied.</param>
        /// <param name="recursePredicate">A predicated accepting a Type and returning a bool which signal to the function if recursion should continue.</param>
        public static void RecursivelyApplyAction(this IEnumerable enumerable, Action<Type, object> action,
                                    Expression<Func<object, bool>> actionPredicate, Expression<Func<Type, bool>> recursePredicate)
        {
            foreach (var item in enumerable.Cast<object>().Where(item => recursePredicate.Compile().Invoke(item.GetType())))
            {
                RecursivelyApplyAction(item, action, actionPredicate, recursePredicate);
            }
        }

        /// <summary>
        ///    This function is used to transerve the object graph of an object and apply an action to them. 
        ///    This is done by recursively inspecting the public properties of the 'current' object.
        /// 
        ///     Conditions:
        ///     1. The function will not step into the properties of CLR Types.
        ///     2. The function checks if properties refer back to the parent, if so, these properties are ignored.
        ///     3. A recursePredicate can be provided to allow control over whether the function should continue transversing given that it has passed condition 1 & 2.
        ///     4. In the special case of encountering an type of IEnumerable, the function will enumerate over the members of the collection.
        ///     5. In the special case of a Dictionary the function will enumerate the KeyValue pairs and inspect both Key and Value.
        ///     
        ///     Prerequisites:
        ///     1. The Type must be a class.
        /// </summary>
        /// <typeparam name="TObject">The Type of the object to be recursed</typeparam>
        /// <param name="object">The instance of the object ot be recursed</param>
        /// <param name="recursePredicate">A predicated accepting a Type and returning a bool which signal to the function if recursion should continue.</param>
        /// <param name="action">An action to apply to a given Type and the actual instance of that Type.</param>
        /// <param name="actionPredicate">A predicate accepting an instance of the current object which returns a bool signalling to the function that the action should be applied.</param>
        private static void RecursivelyApplyAction<TObject>(this TObject @object, Action<Type, object> action,
                                         Expression<Func<object, bool>> actionPredicate, Expression<Func<Type, bool>> recursePredicate) where TObject : class
        {
            if (@object == null)
                throw new ArgumentNullException("object");

            //Should we apply the action?
            if (actionPredicate.Compile().Invoke(@object))
            {
                action.Invoke(@object.GetType(), @object);
            }

            //If its a CLR Type ignore the properties
            if (@object.IsSystemType()) return;

            //Iterate the properties
            foreach (var propertyInfo in @object.GetType().GetProperties())
            {
                var property = @object.GetType().GetProperty(propertyInfo.Name).GetValue(@object, null);

                //If the property refers to the parent just continue with the other properties.
                if (Equals(@object, property))
                    continue;

                //This is a special case for null values, we want to still know the type in the action, 
                //but if its null we wont know the type, hence the added Type argument .
                if (property == null)
                {
                    if (actionPredicate.Compile().Invoke(propertyInfo))
                    {
                        action.Invoke(propertyInfo.PropertyType, null);
                    }
                }

                else if (property is IDictionary)
                {
                    RecursivelyApplyAction(property as IDictionary, action, actionPredicate, recursePredicate);
                }

                else if (property is IEnumerable)
                {
                    RecursivelyApplyAction(property as IEnumerable, action, actionPredicate, recursePredicate);
                }

                else if (!property.IsSystemType())
                {
                    if (recursePredicate.Compile().Invoke(propertyInfo.PropertyType))
                    {
                        RecursivelyApplyAction(property, action, actionPredicate, recursePredicate);
                    }
                }
            }
        }

        public static object GetPropertyValue(this object source, string propertyName)
        {
            var type = source.GetType();
            var properties = type.GetProperty(propertyName);

            return properties.GetValue(source, null);
        }

        public static Object CreateGenericTypeWithConstructor(Type genericType, Type genericTypeParameters, Type[] constructorParameters, Object[] constructorArguments)
        {
            var makeGenericType = genericType.MakeGenericType(genericTypeParameters);

            var constructor = makeGenericType.GetConstructor(constructorParameters);

            if (constructor == null)
                throw new Exception(String.Format("An error occured when creating a generic type. No constructor was found on the type [{0}] with " +
                                                  "genericTypeParameters [{1}] that matches the given constructorParameter types (Signature) [{2}]", genericType, genericTypeParameters, constructorParameters));

            return constructor.Invoke(constructorArguments);
        }

        public static object GetDefault(Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }
    }
}