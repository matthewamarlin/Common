﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;

namespace SpearOne.Common.Reflection
{
    public class CallerInfo
    {
        public string CallerMemberName { get; set; }
        public string CallerFilePath { get; set; }
        public int CallerFileLineNumber { get; set; }
        public int CallerFileColumnNumber { get; set; }
        public string CallerDeclaringType { get; set; }

        /// <summary>
        /// This method retrieves the caller information for the caller at a certain depth in the stack frame.
        /// By default the depth is set to 2. This will retrieve the caller information for the method 1 frame above 
        /// where this method is being called from.
        /// </summary>
        /// <param name="depth">The stack frame depth to use when reflecting for method information</param>
        /// <returns>CallerInfo</returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static CallerInfo Get(int depth = 2)
        {
            var stackFrame = new StackTrace(true).GetFrame(depth);
            var methodInfo = stackFrame.GetMethod();
            var memberName = methodInfo.Name;
            var fileLineNumber = stackFrame.GetFileLineNumber();
            var fileColumnNumber = stackFrame.GetFileColumnNumber();
            var filePath = stackFrame.GetFileName();
            string className = null;

            if (methodInfo.DeclaringType != null)
            {
                className = methodInfo.DeclaringType.Name;
            }

            return new CallerInfo
            {
                CallerDeclaringType = className,
                CallerMemberName = memberName,
                CallerFilePath = filePath,
                CallerFileLineNumber = fileLineNumber,
                CallerFileColumnNumber = fileColumnNumber
            };
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine(String.Format("Caller Declaring Type: {0}", CallerDeclaringType));
            sb.AppendLine(String.Format("Caller Member Name: {0}", CallerMemberName));
            sb.AppendLine(String.Format("Caller File Path: {0}", CallerDeclaringType));
            sb.AppendLine(String.Format("Caller File Line Number: {0}", CallerDeclaringType));
            sb.AppendLine(String.Format("Caller File Column Number: {0}", CallerDeclaringType));

            return sb.ToString();
        }
    }
}
