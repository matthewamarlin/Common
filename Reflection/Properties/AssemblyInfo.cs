﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Common.Reflection")]
[assembly: AssemblyDescription("This package contains common code for .Net Reflection.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("0c9f082b-0b88-4573-89be-4bf630d8c7b3")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]