﻿using System;
using System.ComponentModel;
using System.Text;

namespace SpearOne.Common.Utilities
{
    public class EnumUtil
	{
		public static string GetDescription(Enum value)
		{
			var fi = value.GetType().GetField(value.ToString());
			var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

			return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
		}

		public static string GetDescriptions(params Enum[] values)
		{
			var sb = new StringBuilder();

			for (var i = 0; i < values.Length; i++)
			{
			    sb.AppendFormat(i == values.Length - 1 ? "{0}" : "{0},", GetDescription(values[i]));
			}

			return sb.ToString();
		}
	}
}
