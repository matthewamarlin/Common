﻿using System;
using System.Xml;

namespace SpearOne.Common.Xml
{
    public class XmlSanitization
    {
        public static void WriteShallowNode(XmlReader reader, XmlWriter writer)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    writer.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
                    writer.WriteAttributes(reader, true);
                    if (reader.IsEmptyElement)
                    {
                        writer.WriteEndElement();
                    }
                    break;
                case XmlNodeType.Text:
                    if (reader.Value.Length > 0)
                    {
                        writer.WriteString(IsLegalXmlChar(reader.Value));
                        break;
                    }
                    writer.WriteString(reader.Value);
                    break;
                case XmlNodeType.Whitespace:
                case XmlNodeType.SignificantWhitespace:
                    writer.WriteWhitespace(reader.Value);
                    break;
                case XmlNodeType.CDATA:
                    if (reader.Value.Length > 0)
                    {
                        writer.WriteString(IsLegalXmlChar(reader.Value));
                        break;
                    }
                    writer.WriteCData(reader.Value);
                    break;
                case XmlNodeType.EntityReference:
                    writer.WriteEntityRef(reader.Name);
                    break;
                case XmlNodeType.XmlDeclaration:
                case XmlNodeType.ProcessingInstruction:
                    writer.WriteProcessingInstruction(reader.Name, reader.Value);
                    break;
                case XmlNodeType.DocumentType:
                    writer.WriteDocType(reader.Name, reader.GetAttribute("PUBLIC"), reader.GetAttribute("SYSTEM"), reader.Value);
                    break;
                case XmlNodeType.Comment:
                    if (reader.Value.Length > 0)
                    {
                        writer.WriteString(IsLegalXmlChar(reader.Value));
                        break;
                    }
                    writer.WriteComment(reader.Value);
                    break;
                case XmlNodeType.EndElement:
                    writer.WriteFullEndElement();
                    break;
            }
        }

        public static string IsLegalXmlChar(string evalStr)
        {
            return IsLegalXmlChar("1.0", evalStr);
        }

        public static string IsLegalXmlChar(string xmlVersion, string evalStr)
        {
            var finalstr = new System.Text.StringBuilder();

            switch (xmlVersion)
            {
                case "1.1": // http://www.w3.org/TR/xml11/#charsets
                    {
                        foreach (char c in evalStr)
                        {
                            try
                            {
                                if (!(c <= 0x8 || c == 0xB || c == 0xC ||
                                (c >= 0xE && c <= 0x1F) || (c >= 0x7F && c <= 0x84) ||
                                (c >= 0x86 && c <= 0x9F) || c > 0x10FFFF))
                                    finalstr.Append(c);
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        break;
                    }
                case "1.0": // http://www.w3.org/TR/REC-xml/#charsets
                    {
                        foreach (char c in evalStr)
                        {
                            try
                            {
                                if ((c == 0x9 /* == '\t' == 9   */      ||
                                c == 0xA /* == '\n' == 10  */          ||
                                c == 0xD /* == '\r' == 13  */          ||
                                (c >= 0x20 && c <= 0xD7FF) ||
                                (c >= 0xE000 && c <= 0xFFFD) ||
                                (c >= 0x10000 && c <= 0x10FFFF)))
                                    finalstr.Append(c);
                            }
                            catch
                            {
                                //do nothing
                            }
                        }
                        break;
                    }
                default:
                    {
                        throw new ArgumentOutOfRangeException
                            ("xmlVersion", string.Format("'{0}' is not a valid XML version.", xmlVersion));
                    }
            }
            return finalstr.ToString();
        }

        public static string InvalidCharactersInspection(string evalstr)
        {
            var finalstr = new System.Text.StringBuilder();
            foreach (var c in evalstr)
            {
                try
                {
                    int charassci = Convert.ToInt16(c);
                    if (charassci >= 32 && charassci <= 126) //include all printable characters only
                        finalstr.Append(c);
                }
                catch
                {
                    //do nothing
                }
            }
            return finalstr.ToString();
        }
    }
}
