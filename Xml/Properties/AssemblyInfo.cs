﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Spearone.Common.Xml")]
[assembly: AssemblyDescription("This package contains common code for XML based tasks, such as sanitization.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("ad70b096-6482-4eee-ba82-6c77d783111c")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]