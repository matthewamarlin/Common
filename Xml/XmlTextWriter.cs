﻿using System.IO;
using System.Text;

namespace SpearOne.Common.Xml
{
    public class XmlTextWriter : System.Xml.XmlTextWriter
    {
        private bool _skip;

        public XmlTextWriter(TextWriter w)
            : base(w)
        {
            _skip = false;
        }

        public XmlTextWriter(Stream w, Encoding encoding)
            : base(w, encoding)
        {
            _skip = false;
        }

        public XmlTextWriter(string filename, Encoding encoding)
            : base(filename, encoding)
        {
            _skip = false;
        }

        public override void WriteEndAttribute()
        {
            if (_skip)
            {
                _skip = false;
            }
            else
            {
                base.WriteEndAttribute();
            }
        }

        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            if ((prefix == "xmlns") && ((localName == "xsd") || (localName == "xsi")))
            {
                _skip = true;
            }
            else
            {
                base.WriteStartAttribute(prefix, localName, ns);
            }
        }

        public override void WriteStartDocument()
        {
        }

        public override void WriteString(string text)
        {
            if (!_skip)
            {
                base.WriteString(text);
            }
        }
    }
}
