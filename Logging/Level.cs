﻿namespace SpearOne.Common.Logging
{
    /// <summary>
    /// Denotes the Level of severity when logging.
    /// Serves as a wrapper for the log4net.Core.Level enumeration.
    /// </summary>
    public enum Level
    {
        /// <summary>
        /// The <see cref="Off" /> level designates a higher level than all the rest.
        /// </summary>
        Off = int.MaxValue,

        /// <summary>
        /// The <see cref="Emergency" /> level designates very severe error events. 
        /// System unusable, emergencies.
        /// </summary>
        Emergency = 12,

        /// <summary>
        /// The <see cref="Fatal" /> level designates very severe error events 
        /// that will presumably lead the application to abort.
        /// </summary>
        Fatal = 11,

        /// <summary>
        /// The <see cref="Alert" /> level designates very severe error events. 
        /// Take immediate action, alerts.
        /// </summary>
        Alert = 10,

        /// <summary>
        /// The <see cref="Critical" /> level designates very severe error events. 
        /// Critical condition, critical.
        /// </summary>
        Critical = 9,

        /// <summary>
        /// The <see cref="Severe" /> level designates very severe error events.
        /// </summary>
        Severe = 8,

        /// <summary>
        /// The <see cref="Error" /> level designates error events that might 
        /// still allow the application to continue running.
        /// </summary>
        Error = 7,

        /// <summary>
        /// The <see cref="Warn" /> level designates potentially harmful 
        /// situations.
        /// </summary>
        Warn = 6,

        /// <summary>
        /// The <see cref="Notice" /> level designates informational messages 
        /// that highlight the progress of the application at the highest level.
        /// </summary>
        Notice = 5,

        /// <summary>
        /// The <see cref="Info" /> level designates informational messages that 
        /// highlight the progress of the application at coarse-grained level.
        /// </summary>
        Info = 4,

        /// <summary>
        /// The <see cref="Debug" /> level designates fine-grained informational 
        /// events that are most useful to debug an application.
        /// </summary>
        Debug = 3,

        /// <summary>
        /// The <see cref="Trace" /> level designates fine-grained informational 
        /// events that are most useful to debug an application.
        /// </summary>
        Trace = 2,

        /// <summary>
        /// The <see cref="Verbose" /> level designates fine-grained informational 
        /// events that are most useful to debug an application.
        /// </summary>
        Verbose = 1,

        /// <summary>
        /// The <see cref="All" /> level designates the lowest level possible.
        /// </summary>
        All = 0,
    }
}