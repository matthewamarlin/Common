﻿using System;
using System.Collections;
using System.Text;

namespace SpearOne.Common.Logging
{
    public static class LogHelper
    {
        public static string Format(object message, params object[] data)
        {
            if (data == null)
            {
                return message == null ? String.Empty : message.ToString();
            }

            var builder = new StringBuilder();

            builder.AppendLine(String.Format("Message: {0}", message));
            builder.AppendLine("Additional Data: ");

            foreach (var item in data)
            {
                if (item is IDictionary)
                {
                    builder.AppendLine(FormatDictionary(item as IDictionary));
                }
                else if (item is IEnumerable)
                {
                    foreach (var subItem in (item as IEnumerable))
                    {
                        builder.AppendLine(subItem.ToString());
                    }
                }
                else
                {
                    builder.AppendLine(item.ToString());
                }
            }

            return builder.ToString();
        }

        private static string FormatDictionary(IDictionary dictionary)
        {
            var builder = new StringBuilder();

            foreach (var keyValuePair in dictionary)
            {
                var key = keyValuePair.GetType().GetProperty("Key").GetValue(keyValuePair, null);
                var value = keyValuePair.GetType().GetProperty("Value").GetValue(keyValuePair, null);

                builder.AppendLine(String.Format("(\"{0}\",\"{1}\")", key, value));
            }

            return builder.ToString();
        }

        public static string Format(object message, Exception exception, params object[] data)
        {
            if (exception == null)
            {
                return Format(message, data);
            }

            return Format(message, data) + Environment.NewLine + exception;
        }
    }
}
