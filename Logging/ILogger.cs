﻿using System;

namespace SpearOne.Common.Logging
{
    public interface ILogger<T> : ILogger where T: class
    { }

    /// <summary>
    /// A common interface for application logging.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Writes a message to the log file with the given severity 'Level'.
        /// If an exception is specified it will be written to the log file 
        /// on a subsequent line, by calling its ToString method.
        /// </summary>
        /// <param name="level">The serverity Level to use.</param>
        /// <param name="message">The message to write.</param>
        /// <param name="exception">An optional exception.</param>
        void Write(Level level, object message, Exception exception = null);

        /// <summary>
        /// Writes an exception to the log file with the given severity 'Level'.
        /// </summary>
        /// <param name="level">The serverity Level to use.</param>
        /// <param name="exception">The exception to write to the log.</param>
        void Write(Level level, Exception exception);


        /// <summary>
        /// Writes a message to the log file with the given severity 'Level' and Data.
        /// The additional data will be enumerated and the <c>ToString</c> method will 
        /// be called on each item.
        /// </summary>
        /// <param name="level">The serverity Level to use.</param>
        /// <param name="message">The message to write.</param>
        /// <param name="data">Data to append to the log message</param>
        void WriteData(Level level, object message, params object[] data);

        /// <summary>
        /// Writes a message to the log file with the given severity 'Level', Exception and Data.
        /// The additional data will be enumerated and the <c>ToString</c> method will be called on each item.
        /// </summary>
        /// <param name="level">The serverity Level to use.</param>
        /// <param name="message">The message to write.</param>
        /// <param name="exception">A System.Exception</param>
        /// <param name="data">Data to append to the log message</param>
        void WriteData(Level level, object message, Exception exception, params object[] data);

        /// <summary>
        /// Writes a message to the log file with the given severity 'Level'.
        /// This method uses the <c>String.Format</c> method to format the message.
        /// </summary>
        /// <param name="level">The serverity Level to use.</param>
        /// <param name="exception">A System.Exception</param>
        /// <param name="format">The portion specifying the Format of the Log message</param>
        /// <param name="args">The arguments to the Format specification</param>
        void WriteFormat(Level level, Exception exception, string format, params object[] args);

        /// <summary>
        /// Writes a formatted message to the log file with the given severity 'Level'.
        /// This method uses the <c>String.Format</c> method to format the message.
        /// </summary>
        /// <param name="level">The serverity Level to use.</param>
        /// <param name="format">The portion specifying the Format of the Log message</param>
        /// <param name="args">The arguments to the Format specification</param>
        void WriteFormat(Level level, string format, params object[] args);

        #region Backward Compatibility - Remove in next iteration

        [Obsolete("Error is deprecated please use Write instead.")]
        void Error(object message, Exception exception = null);
        [Obsolete("ErrorData is deprecated please use WriteData instead.")]
        void ErrorData(object message, params object[] data);
        [Obsolete("ErrorData is deprecated please use WriteData instead.")]
        void ErrorData(object message, Exception exception, params object[] data);
        [Obsolete("ErrorFormat is deprecated please use WriteFormat instead.")]
        void ErrorFormat(string format, params object[] args);

        [Obsolete("Warn is deprecated please use Write instead.")]
        void Warn(object message, Exception exception = null);
        [Obsolete("WarnData is deprecated please use WriteData instead.")]
        void WarnData(string message, params object[] data);
        [Obsolete("WarnData is deprecated please use WriteData instead.")]
        void WarnData(string message, Exception exception, params object[] data);
        [Obsolete("WarnFormat is deprecated please use WriteFormat instead.")]
        void WarnFormat(string format, params object[] args);

        [Obsolete("Info is deprecated please use Write instead.")]
        void Info(object message, Exception exception = null);
        [Obsolete("InfoData is deprecated please use WriteData instead.")]
        void InfoData(object message, params object[] data);
        [Obsolete("InfoData is deprecated please use WriteData instead.")]
        void InfoData(object message, Exception exception, params object[] data);
        [Obsolete("InfoFormat is deprecated please use WriteFormat instead.")]
        void InfoFormat(string format, params object[] data);

        [Obsolete("Debug is deprecated please use Write instead.")]
        void Debug(object message, Exception exception = null);
        [Obsolete("DebugData is deprecated please use WriteData instead.")]
        void DebugData(object message, params object[] data);
        [Obsolete("DebugData is deprecated please use WriteData instead.")]
        void DebugData(object message, Exception exception, params object[] data);
        [Obsolete("DebugFormat is deprecated please use WriteFormat instead.")]
        void DebugFormat(string format, params object[] args);

        #endregion

    }
}