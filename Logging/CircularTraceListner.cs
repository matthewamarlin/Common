﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace AbsoluteSystems.Calypso.Logging
{
    public class CircularTraceListener : XmlWriterTraceListener
    {
        static CircularStream _stream;
        bool _maxQuotaInitialized;
        const string FileQuotaAttribute = "maxFileSizeKB";
        const long DefaultMaxQuota = 1000;
        const string DefaultTraceFile = "Service Trace.svclog";

        #region Member Functions

        private long MaxQuotaSize
        {
            //Get the MaxQuotaSize from configuration file
            //Set to Default Value if there are any problems

            get
            {
                long maxFileQuota = 0;
                if (!_maxQuotaInitialized)
                {
                    try
                    {
                        var maxQuotaOption = Attributes[FileQuotaAttribute];
                        maxFileQuota = maxQuotaOption == null ? DefaultMaxQuota : int.Parse(maxQuotaOption, CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        maxFileQuota = DefaultMaxQuota;
                    }
                    finally
                    {
                        _maxQuotaInitialized = true;
                    }
                }

                if (maxFileQuota <= 0)
                {
                    maxFileQuota = DefaultMaxQuota;
                }

                //MaxFileQuota is in KB in the configuration file, convert to bytes

                maxFileQuota = maxFileQuota * 1024;
                return maxFileQuota;
            }
        }

        private void DetermineOverQuota()
        {

            //Set the MaxQuota on the stream if it hasn't been done

            if (!_maxQuotaInitialized)
            {
                _stream.MaxQuotaSize = MaxQuotaSize;
            }

            //If we're past the Quota, flush, then switch files

            if (_stream.IsOverQuota)
            {
                base.Flush();
                _stream.SwitchFiles();
            }
        }

        #endregion

        #region XmlWriterTraceListener Functions

        public CircularTraceListener(string file)
            : base(_stream = new CircularStream(file))
        {
        }

        public CircularTraceListener()
            : base(_stream = new CircularStream(DefaultTraceFile))
        {
        }

        protected override string[] GetSupportedAttributes()
        {
            return new[] { FileQuotaAttribute };
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            DetermineOverQuota();
            base.TraceData(eventCache, source, eventType, id, data);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
        {
            DetermineOverQuota();
            base.TraceEvent(eventCache, source, eventType, id);
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
        {
            DetermineOverQuota();
            base.TraceData(eventCache, source, eventType, id, data);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            DetermineOverQuota();
            base.TraceEvent(eventCache, source, eventType, id, format, args);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
        {
            DetermineOverQuota();
            base.TraceEvent(eventCache, source, eventType, id, message);
        }

        public override void TraceTransfer(TraceEventCache eventCache, string source, int id, string message, Guid relatedActivityId)
        {
            DetermineOverQuota();
            base.TraceTransfer(eventCache, source, id, message, relatedActivityId);

        }

        #endregion
    }

    public class CircularStream : Stream
    {
        private readonly FileStream[] FStream;
        private readonly String[] FPath;
        private long _dataWritten;
        private long _fileQuota;
        private int _currentFile;

        public CircularStream(string fileName)
        {
            //Handle all exceptions within this class, since tracing shouldn't crash a service
            //Add 00 and 01 to FileNames and open streams

            try
            {
                var filePath = Path.GetDirectoryName(fileName);
                var fileBase = Path.GetFileNameWithoutExtension(fileName);
                var fileExt = Path.GetExtension(fileName);

                if (string.IsNullOrEmpty(filePath))
                {
                    filePath = AppDomain.CurrentDomain.BaseDirectory;
                }

                FPath = new String[2];
                FPath[0] = Path.Combine(filePath, fileBase + " 1" + fileExt);
                FPath[1] = Path.Combine(filePath, fileBase + " 2" + fileExt);

                FStream = new FileStream[2];
                FStream[0] = new FileStream(FPath[0], FileMode.Create);
            }
            catch
            {
                //Supress Exceptions
            }

        }

        public long MaxQuotaSize
        {
            get
            {
                return _fileQuota;
            }
            set
            {
                _fileQuota = value;
            }
        }

        public void SwitchFiles()
        {
            try
            {
                //Close current file, open next file (deleting its contents)

                _dataWritten = 0;
                FStream[_currentFile].Close();

                _currentFile = (_currentFile + 1) % 2;

                FStream[_currentFile] = new FileStream(FPath[_currentFile], FileMode.Create);
            }
            catch (Exception)
            {
                //Supress Exceptions
            }
        }

        public bool IsOverQuota
        {
            get
            {
                return (_dataWritten >= _fileQuota);
            }

        }

        public override bool CanRead
        {
            get
            {
                try
                {
                    return FStream[_currentFile].CanRead;
                }
                catch (Exception)
                {
                    return true;
                }
            }
        }

        public override bool CanSeek
        {
            get
            {
                try
                {
                    return FStream[_currentFile].CanSeek;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public override long Length
        {
            get
            {
                try
                {
                    return FStream[_currentFile].Length;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        public override long Position
        {
            get
            {
                try
                {
                    return FStream[_currentFile].Position;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                try
                {
                    FStream[_currentFile].Position = Position;
                }
                catch (Exception)
                {
                    //Supress Exceptions
                }
            }
        }

        public override bool CanWrite
        {
            get
            {
                try
                {
                    return FStream[_currentFile].CanWrite;
                }
                catch (Exception)
                {
                    return true;
                }
            }
        }

        public override void Flush()
        {
            try
            {
                FStream[_currentFile].Flush();
            }
            catch (Exception)
            {
                //Supress Exceptions
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            try
            {
                return FStream[_currentFile].Seek(offset, origin);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public override void SetLength(long value)
        {
            try
            {
                FStream[_currentFile].SetLength(value);
            }
            catch (Exception)
            {
                //Supress Exceptions
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            try
            {
                //Write to current file

                FStream[_currentFile].Write(buffer, offset, count);
                _dataWritten += count;

            }
            catch (Exception)
            {
                //Supress Exceptions
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            try
            {
                return FStream[_currentFile].Read(buffer, offset, count);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public override void Close()
        {
            try
            {
                FStream[_currentFile].Close();
            }
            catch (Exception)
            {
                //Supress Exceptions
            }
        }
    }

}
