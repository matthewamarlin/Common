﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.Common.Serialization")]
[assembly: AssemblyDescription("This package contains common code for Serialization, such as DataContract and XML Serialization.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("ea03a8f0-8c0b-4f99-8107-7811e536a34f")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0")]