﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using SpearOne.Common.Xml;

namespace SpearOne.Common.Serialization
{
    /// <summary>
    /// Class to enable serilization/deserialization of Attributed members
    /// </summary>
    public class DataContractSerialization
    {
        /// <summary>
        /// Method to serialize object to XML string
        /// Only members marked with appropriate attributes eg.[DataContract] are considered
        /// </summary>
        /// <returns>XML string representing object</returns>
        public static string Serialize(object o)
        {
            return Serialize(o, false);
        }

        public static string Serialize(object o, bool checkInvalidCharacters)
        {

            var serializer = new DataContractSerializer(o.GetType());

            var stream = new MemoryStream();

            serializer.WriteObject(stream, o);

            stream.Seek(0, SeekOrigin.Begin);

            string xml;
            var sb = new StringBuilder();

            if (!checkInvalidCharacters)
            {
                var srdr = XmlReader.Create(stream);

                srdr.MoveToContent();

                xml = srdr.ReadOuterXml();
            }
            else
            {
                using (var reader = XmlReader.Create(stream, new XmlReaderSettings { CheckCharacters = false }))
                {
                    using (var writer = XmlWriter.Create(sb, new XmlWriterSettings
                    {
                        CheckCharacters = false,
                        OmitXmlDeclaration = true,
                        Indent = false
                    }))
                    {
                        while (reader.Read())
                        {
                            XmlSanitization.WriteShallowNode(reader, writer);
                        }
                        writer.Close();
                    }
                    reader.Close();
                }

                xml = sb.ToString();
            }

            return xml;
        }

        /// <summary>
        /// Static method to deserialize XML string to object type T
        /// </summary>
        /// <typeparam name="T">The type that the operation will return</typeparam>
        /// <param name="xml">The XML string</param>
        /// <returns>Object of type T</returns>
        public static T Deserialize<T>(string xml) where T : class
        {
            var serializer = new DataContractSerializer(typeof(T));

            var reqRdr = XmlReader.Create(new StringReader(xml));

            return (T)serializer.ReadObject(reqRdr);
        }

        public static object Deserialize(string xml, Type tp)
        {
            var serializer = new DataContractSerializer(tp);

            var reqRdr = XmlReader.Create(new StringReader(xml));

            return serializer.ReadObject(reqRdr);
        }
    }
}
