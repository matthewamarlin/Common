﻿using System;
using System.Globalization;
using System.Threading;
using log4net;
using log4net.Core;
using log4net.Util;
using SpearOne.Common.Reflection;

namespace SpearOne.Common.Logging.Log4Net
{
    public class Log4NetWrapper<T> : Log4NetWrapper, ILogger<T> where T: class
    {
        public Log4NetWrapper() : base(typeof(T))
        { }
    }

    public class Log4NetWrapper : ILogger
    {
        private readonly log4net.Core.ILogger _logger;

        public Log4NetWrapper(Type declaringType)
        {
            _logger = LoggerManager.GetLogger(declaringType.Assembly, declaringType);
        }
        
        public void Write(Level level,
            Exception exception)
        {
            Log(Convert(level), null, exception, Convert(CallerInfo.Get()));
        }

        public void Write(Level level,
            object message,
            Exception exception = null)
        {
            Log(Convert(level), message, exception, Convert(CallerInfo.Get()));
        }

        public void WriteData(Level level,
            object message,
            params object[] data)
        {
            Log(Convert(level), LogHelper.Format(message, data), null, Convert(CallerInfo.Get()));
        }

        public void WriteData(Level level,
            object message,
            Exception exception,
            params object[] data)
        {
            Log(Convert(level), LogHelper.Format(message, data), exception, Convert(CallerInfo.Get()));
        }

        public void WriteFormat(Level level,
            string format,
            params object[] args)
        {
            Log(Convert(level), new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, Convert(CallerInfo.Get()));
        }

        public void WriteFormat(Level level,
            Exception exception,
            string format,
            params object[] args)
        {
            Log(Convert(level), new SystemStringFormat(CultureInfo.InvariantCulture, format, args), exception, Convert(CallerInfo.Get()));
        }

        private void Log(log4net.Core.Level level,
            object message,
            Exception exception,
            LocationInfo callerInfo)
        {
            var timeStamp = DateTime.Now;

            if (!ShouldLogAt(level))
                return;

            var exceptionString = exception == null ? null : exception.ToString();

            var logData = new LoggingEventData
            {
                Domain = AppDomain.CurrentDomain.FriendlyName,
                ExceptionString = exceptionString,
                Identity = Thread.CurrentPrincipal.Identity.Name,
                Level = level,
                LocationInfo = callerInfo,
                LoggerName = _logger.Name,
                Message = (message == null ? "null" : message.ToString()),
                //Properties = , //Add more custom properties if needed.
                ThreadName = Thread.CurrentThread.Name,
                TimeStamp = timeStamp,
                UserName = Thread.CurrentPrincipal.Identity.Name,
            };

            _logger.Log(new LoggingEvent(logData));
        }

        private bool ShouldLogAt(log4net.Core.Level level)
        {
            var logThreashold = ((log4net.Repository.Hierarchy.Logger)_logger).Parent.Level;
            var shouldLog = level >= logThreashold;
            return shouldLog;
        }

        private log4net.Core.Level Convert(Level level)
        {
            switch (level)
            {
                case Level.Alert:
                    return log4net.Core.Level.Alert;

                case Level.All:
                    return log4net.Core.Level.All;

                case Level.Critical:
                    return log4net.Core.Level.Critical;

                case Level.Debug:
                    return log4net.Core.Level.Debug;

                case Level.Emergency:
                    return log4net.Core.Level.Emergency;

                case Level.Error:
                    return log4net.Core.Level.Error;

                case Level.Fatal:
                    return log4net.Core.Level.Fatal;

                case Level.Info:
                    return log4net.Core.Level.Info;

                case Level.Notice:
                    return log4net.Core.Level.Notice;

                case Level.Off:
                    return log4net.Core.Level.Off;

                case Level.Severe:
                    return log4net.Core.Level.Severe;

                case Level.Trace:
                    return log4net.Core.Level.Trace;

                case Level.Verbose:
                    return log4net.Core.Level.Verbose;

                case Level.Warn:
                    return log4net.Core.Level.Warn;

                default:
                    throw new NotImplementedException(String.Format("A mapping for the Type [{0}] with value [{1}] to [log4Net.Core.Level] has not been implemented. ", typeof(Level).FullName, level));
            }
        }

        private LocationInfo Convert(CallerInfo callerInfo)
        {
            return new LocationInfo(callerInfo.CallerDeclaringType,
                callerInfo.CallerMemberName,
                callerInfo.CallerFilePath,
                callerInfo.CallerFileLineNumber.ToString(CultureInfo.InvariantCulture));
        }

        #region Backward Compatibility

        public void Error(object message, Exception exception = null)
        {
            Log(log4net.Core.Level.Error, message, exception, Convert(CallerInfo.Get()));
        }

        public void ErrorData(object message, params object[] data)
        {
            Log(log4net.Core.Level.Error, LogHelper.Format(message, data), null, Convert(CallerInfo.Get()));
        }

        public void ErrorData(object message, Exception exception, params object[] data)
        {
            Log(log4net.Core.Level.Error, LogHelper.Format(message, data), exception, Convert(CallerInfo.Get()));
        }

        public void ErrorFormat(string format, params object[] args)
        {
            Log(log4net.Core.Level.Error, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, Convert(CallerInfo.Get()));
        }

        public void Warn(object message, Exception exception = null)
        {
            Log(log4net.Core.Level.Warn, message, exception, Convert(CallerInfo.Get()));
        }

        public void WarnData(string message, params object[] data)
        {
            Log(log4net.Core.Level.Warn, LogHelper.Format(message, data), null, Convert(CallerInfo.Get()));
        }

        public void WarnData(string message, Exception exception, params object[] data)
        {
            Log(log4net.Core.Level.Warn, LogHelper.Format(message, data), exception, Convert(CallerInfo.Get()));
        }

        public void WarnFormat(string format, params object[] args)
        {
            Log(log4net.Core.Level.Warn, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, Convert(CallerInfo.Get()));
        }

        public void Info(object message, Exception exception = null)
        {
            Log(log4net.Core.Level.Info, message, exception, Convert(CallerInfo.Get()));
        }

        public void InfoData(object message, params object[] data)
        {
            Log(log4net.Core.Level.Info, LogHelper.Format(message, data), null, Convert(CallerInfo.Get()));
        }

        public void InfoData(object message, Exception exception, params object[] data)
        {
            Log(log4net.Core.Level.Info, LogHelper.Format(message, data), exception, Convert(CallerInfo.Get()));
        }

        public void InfoFormat(string format, params object[] data)
        {
            Log(log4net.Core.Level.Info, new SystemStringFormat(CultureInfo.InvariantCulture, format, data), null, Convert(CallerInfo.Get()));
        }

        public void Debug(object message, Exception exception = null)
        {
            Log(log4net.Core.Level.Debug, message, exception, Convert(CallerInfo.Get()));
        }

        public void DebugData(object message, params object[] data)
        {
            Log(log4net.Core.Level.Debug, LogHelper.Format(message, data), null, Convert(CallerInfo.Get()));
        }

        public void DebugData(object message, Exception exception, params object[] data)
        {
            Log(log4net.Core.Level.Debug, LogHelper.Format(message, data), exception, Convert(CallerInfo.Get()));
        }

        public void DebugFormat(string format, params object[] args)
        {
            Log(log4net.Core.Level.Debug, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, Convert(CallerInfo.Get()));
        }

        #endregion
    }
}
